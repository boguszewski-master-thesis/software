#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QFile>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    lz77.set_progress_bar(ui->progressBar_compress);

    //LZ77
    connect(ui->pushButton_open_path, &QPushButton::clicked, &lz77, &Lz77::show_dialog);
    connect(ui->pushButton_compress, &QPushButton::clicked, &lz77, &Lz77::compres_data);
    connect(&lz77, &Lz77::path_changed, ui->lineEdit_raw_path, &QLineEdit::setText);
    connect(&settings, &Settings::coms_selected_changed, &lz77, &Lz77::com_selected_change);
    connect(ui->lineEdit_raw_path, &QLineEdit::textEdited, this, &MainWindow::raw_path_update );
    connect(lz77.get_logger(), &Logger::send_message, ui->textEdit_lz77, &QTextEdit::append);

    //Settings
    connect(&settings, &Settings::coms_list_changed, this, &MainWindow::update_coms);
    connect(ui->pushButton_refresh_coms, &QPushButton::clicked, &settings, &Settings::coms_refresh);
    connect(ui->comboBox_coms, QOverload<int>::of(&QComboBox::currentIndexChanged), &settings, &Settings::com_selected_changed);

}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * @brief update combobox with coms via Settings class
 */
void MainWindow::update_coms()
{
    settings.update_coms_checkbox(ui->comboBox_coms);
}

void MainWindow::raw_path_update(const QString &text)
{
    if(QFile::exists(text))  emit lz77.path_changed(text);
}
