/*
 * frame.h
 *
 *  Created on: 16 Nov 2021
 *      Author: robsk
 */

#ifndef INC_FRAME_H_
#define INC_FRAME_H_

/**
 * @brief struct describe sub-command connected with uart_cmd_lz77 command
 */
enum  class lz77_sub_cmd{
    start,
    append,
    end
};


/**
 * @brief command sended to device
 */
enum class command{
    lz77,
    lzss
};

/**
 * @brief subcommand sended to device
 */
typedef union sub_cmd{
    lz77_sub_cmd lz77;
}sub_cmd_t;

/**
 *  @brief comunication frame
 */
typedef struct frame_struct{

	struct{
        command cmd;
        sub_cmd_t sub_cmd;
	}head;

	uint8_t *data;
	uint8_t crc;
}frame_struct_t;

#endif /* INC_FRAME_H_ */
