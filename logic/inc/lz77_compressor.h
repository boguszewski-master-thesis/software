﻿#ifndef LZ77COMPRESS_H
#define LZ77COMPRESS_H

#include <QThread>
#include <QSerialPort>
#include "logger.h"

class Lz77Compressor : public QThread
{
    Q_OBJECT
public:
    explicit Lz77Compressor(Logger &logger, const QSerialPortInfo &port_info, const QString &input_file, QObject *parent = nullptr);
    ~Lz77Compressor();
public slots:

signals:
    void update_progress_bar(int value);

protected:
    void run() override;

private:
    const int DATA_PARTS_SIZE = 250;    // how many byte is sendend to compress in one chunk

    QSerialPort serial;
    const QString &input_file_path;     // path to file with raw data
    Logger &logger;
    QByteArray data_in;                 // raw input data
    QByteArray data_out;                // compressed output data

    bool read_input_data();

    bool setup_compression_mode(void);
    bool compress_data(void);
    void progress_update(int index);
};

#endif // LZ77COMPRESS_H
