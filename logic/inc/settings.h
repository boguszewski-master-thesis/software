#ifndef SETTINGS_H
#define SETTINGS_H

#include <QWidget>
#include <QTimer>
#include <QSerialPortInfo>
#include <QComboBox>

class Settings : public QWidget
{
    Q_OBJECT
public:
    explicit Settings(QWidget *parent = nullptr);
    void update_coms_checkbox(QComboBox *coms_checkbox);

signals:
    void coms_list_changed();
    void coms_selected_changed(const QSerialPortInfo &port_info);

public slots:
    void coms_refresh();
    void com_selected_changed(int index);

private slots:
    void com_search();

private:
    QTimer com_search_timer;
    QList<QSerialPortInfo> coms;    // List of all availavble coms
    QSerialPortInfo com_selected;   // com selected to communication

    bool is_coms_changed(const QList<QSerialPortInfo> &new_coms);
    void coms_update(const QList<QSerialPortInfo> &new_coms);
};

#endif // SETTINGS_H
