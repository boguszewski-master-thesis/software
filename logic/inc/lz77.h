#ifndef LZ77_H
#define LZ77_H

#include <QWidget>
#include <QProgressBar>

#include "lz77_compressor.h"
#include "settings.h"
#include "logger.h"

class Lz77 : public QWidget
{
    Q_OBJECT
public:
    explicit Lz77( QWidget *parent = nullptr);

    void set_progress_bar(QProgressBar *progress_bar);
    Logger *get_logger(void);

public slots:
    void show_dialog(void);
    void com_selected_change(const QSerialPortInfo &port_info);
    void raw_path_change(const QString &path);
    void compres_data();

signals:
    void path_changed(const QString &path);

private:
    QProgressBar *progress_bar = NULL;

    QSerialPortInfo port_info;
    QString raw_file_path;
    Lz77Compressor *compressor_thread = NULL;
    Logger logger;

};

#endif // LZ77_H
