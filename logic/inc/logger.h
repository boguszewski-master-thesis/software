#ifndef LOGGER_H
#define LOGGER_H

#include <QWidget>

class Logger : public QWidget
{
    Q_OBJECT
public:
    explicit Logger(QWidget *parent = nullptr);
    void log(const QString &text);
    void log(const QString &text, const QString &argument);
    void error(const QString &text);

signals:
    void send_message(const QString &text);

private:
    QString get_current_time_formatted(void);
    QString get_time_span(void);

};

#endif // LOGGER_H
