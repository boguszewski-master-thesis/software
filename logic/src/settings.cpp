#include "settings.h"
#include <QDebug>

Settings::Settings(QWidget *parent) : QWidget(parent)
{
    // Setup searching for new ports
    connect(&com_search_timer, &QTimer::timeout, this, &Settings::com_search);
    com_search_timer.setInterval(500);
    com_search_timer.start();
}

/**
 * @brief Update checkbox coms view
 * @param coms_checkbox pointer to combobox to edit
 */
void Settings::update_coms_checkbox(QComboBox *coms_checkbox)
{
    QString device;
    coms_checkbox->clear();

    for(int i=0; i<coms.count(); i++)
    {
        device = coms[i].portName();
        if(coms[i].description().isEmpty() == false)
        {
            device += " [ " + coms[i].description() + " ] ";
        }
        coms_checkbox->addItem(device);
    }
}

void Settings::coms_refresh()
{
    coms_update(QSerialPortInfo::availablePorts());
}

void Settings::com_selected_changed(int index)
{
    if(index < 0 ) return;

    if(com_selected.portName() != coms[index].portName())
    {
        com_selected = coms[index];
        emit coms_selected_changed(com_selected);
    }
}

/**
 * @brief Search for new ports
 */
void Settings::com_search()
{
    qDebug() << "Coms update";
    auto new_ports = QSerialPortInfo::availablePorts();

    if(is_coms_changed(new_ports)) coms_update(new_ports);
}

/**
 * @brief Compare new list port with all and check if something changed
 * @param new_coms - new list with serialPortInfo to compare with coms
 * @return true - lista urzadzen sie zmienila, false - liczba urzadzen pozostala taka sama
 */
bool Settings::is_coms_changed(const QList<QSerialPortInfo> &new_coms)
{
    bool same_found;
    if(new_coms.count() != coms.count()) return true;

    for(int i=0; i<new_coms.count(); i++)
    {
        same_found = false;

        for(int j=0; j<coms.count(); j++)
        {
            if(new_coms[i].portName() == coms[i].portName())
            {
                same_found = true;
                break;
            }
        }

        if(same_found == false) return true;
    }

    return false;
}

/**
 * @brief Change coms for new one, send signal that it changed
 * @param new_coms
 */
void Settings::coms_update(const QList<QSerialPortInfo> &new_coms)
{
    coms.clear();
    coms.append(new_coms);
    emit coms_list_changed();
}
