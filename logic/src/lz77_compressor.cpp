#include "lz77_compressor.h"
#include <QFile>

#include "frame.h"

/**
 *  LOG MESSAGES
 */
#define START_LZ77_TEXT             "Start LZ77 compression"
#define FILE_LOADED_TEXT            "File loaded: ", input_file_path
#define CANT_OPEN_TEXT              "Can't open connection"
#define CON_OPENED_TEXT             "Connection opened: ", serial.portName()
#define CON_STM32_ERROR_TEXT        "Can't communicate with STM32"
#define SETUP_TEXT                  "Device setup in LZ77 mode"
#define START_COMPRESSION_TEXT      "Start compressing"
#define END_COMPRESSION_TEXT        "Compression ended!"
#define RAW_SIZE_PRINT_TEXT         "Size raw: ",        QString::number(data_in.count())
#define COMPRESS_SIZE_PRINT_TEXT    "Size compressed: ", QString::number(data_out.count())
#define SUCCESS_TEXT                "Succesfull!"

Lz77Compressor::Lz77Compressor(Logger &logger,
                               const QSerialPortInfo &port_info,
                               const QString &input_file, QObject *parent)
    :logger(logger), input_file_path(input_file), QThread(parent)
{
    setPriority(QThread::Priority::HighPriority);
    serial.setPort(port_info);
    serial.setBaudRate(300E3);
}

Lz77Compressor::~Lz77Compressor()
{
    wait();
}

void Lz77Compressor::run()
{
    emit update_progress_bar(0);
    logger.log(START_LZ77_TEXT);

    if(! read_input_data()) return;
    logger.log(FILE_LOADED_TEXT);


    if(!serial.open(QSerialPort::ReadWrite))
    {
        //auto error = serial.error();
        logger.error(CANT_OPEN_TEXT);
        return;
    }
    logger.log(CON_OPENED_TEXT);

    if(setup_compression_mode() == false)
    {
        logger.error(CON_STM32_ERROR_TEXT);
        serial.close();
        return;
    }
    logger.log(SETUP_TEXT);


    logger.log(START_COMPRESSION_TEXT);
    if(compress_data() == false)
    {
        serial.close();
        return;
    }
    logger.log(END_COMPRESSION_TEXT);

    logger.log(RAW_SIZE_PRINT_TEXT);
    logger.log(COMPRESS_SIZE_PRINT_TEXT);

    logger.log(SUCCESS_TEXT);

    serial.close();

    update_progress_bar(100);
}

inline bool Lz77Compressor::read_input_data()
{
    QFile file(input_file_path);

    //Check if path is pased
    if(input_file_path.isEmpty()) return false;

    //try to open file
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return false;

    data_in = file.readAll();
    file.close();

    return true;
}

bool Lz77Compressor::setup_compression_mode(void)
{
    const char head[] = {(char)command::lz77, (char)lz77_sub_cmd::start};
    QByteArray response;

    serial.write(head, sizeof (head));
    serial.waitForBytesWritten();

    if(serial.waitForReadyRead(50))
    {
        do
        {
            response.append(serial.readAll());
        }while(serial.waitForReadyRead(10));

    }
    else
    {
        return false;
    }

    return true;
}

uint16_t CRC16_2(uint8_t *buf, int len)
{
  uint16_t crc = 0xFFFF;
  for (int pos = 0; pos < len; pos++)
  {
  crc ^= (uint16_t)buf[pos];    // XOR byte into least sig. byte of crc

  for (int i = 8; i != 0; i--) {    // Loop over each bit
    if ((crc & 0x0001) != 0) {      // If the LSB is set
      crc >>= 1;                    // Shift right and XOR 0xA001
      crc ^= 0xA001;
    }
    else                            // Else LSB is not set
      crc >>= 1;                    // Just shift right
    }
  }

  return crc;
}

uint16_t crc;

bool Lz77Compressor::compress_data(void)
{
    const char head[] = {(char)command::lz77, (char)lz77_sub_cmd::append};
    QByteArray response;
    int i;
    response.reserve(DATA_PARTS_SIZE * 3 + 10);

    //clear buffer
    serial.flush();

    for(int i=0; i<data_in.count(); i += DATA_PARTS_SIZE)
    {
        response.clear();

        serial.write(head, sizeof (head));
        serial.waitForBytesWritten();

        if(i + DATA_PARTS_SIZE < data_in.count()) serial.write(data_in.data() + i, DATA_PARTS_SIZE);
        else                                serial.write(data_in.data() + i, data_in.count() - i);

        serial.waitForBytesWritten();
        //sserial.flush();

        if(serial.waitForReadyRead(1500))
        {
            do
            {
                response.append(serial.readAll());

            }while(serial.waitForReadyRead(20));

        }
        else
        {
            return false;
        }

        if(!response.mid(0,2).contains(head)) return false;

        progress_update(i);
        response.remove(0,2);
        response.remove(response.count() - 2, 2);
        data_out.append(response);
    }

    return true;
}

/**
 * @brief Update progress bar
 * @param index index of data analyzed
 */
void Lz77Compressor::progress_update(int index)
{
    int progress = index * 100 / data_in.count();
    emit update_progress_bar(progress);
}
