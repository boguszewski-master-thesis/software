#include "lz77.h"
#include <QFileDialog>
#include <QStandardPaths>
#include <QDebug>
#include <QFile>
#include <QTime>

Lz77::Lz77(QWidget *parent) : QWidget(parent)
{
    connect(this, &Lz77::path_changed, this, &Lz77::raw_path_change);
}

void Lz77::set_progress_bar(QProgressBar *new_progress_bar)
{
    progress_bar = new_progress_bar;
}

Logger* Lz77::get_logger(void)
{
    return &logger;
}

/**
 * @brief Show QFileDialog and update path
 */
void Lz77::show_dialog(void)
{
    QFileDialog dialog;

    dialog.setViewMode(QFileDialog::ViewMode::Detail);
    dialog.setNameFilter("Raw text data *.txt (*.txt);; All files (*.*)");
    dialog.setDirectory(QStandardPaths::standardLocations(QStandardPaths::HomeLocation).last());
    dialog.exec();

    if(dialog.result() == QFileDialog::Accepted)
    {
        emit path_changed(dialog.selectedFiles()[0]);
    }
}

void Lz77::com_selected_change(const QSerialPortInfo &new_port_info)
{
    port_info = new_port_info;
}

void Lz77::raw_path_change(const QString &path)
{
    raw_file_path = path;
}

void Lz77::compres_data()
{
    if(compressor_thread != NULL)
    {
        if(compressor_thread->isRunning()) return;
        delete compressor_thread;
    }

    compressor_thread = new Lz77Compressor(logger, port_info, raw_file_path);

    connect(compressor_thread, &Lz77Compressor::update_progress_bar, progress_bar, &QProgressBar::setValue);
    compressor_thread->start();
}
