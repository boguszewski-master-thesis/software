#include "logger.h"
#include <QTime>

Logger::Logger(QWidget *parent) : QWidget(parent)
{

}

void Logger::log(const QString &text)
{
    QString output;
    QString time_span = get_time_span();

    output.append(" <div> ");

    output.append(time_span);

    output.append("<span> ");
    output.append(text);
    output.append(" </span> ");

    output.append(" </div> ");
    emit send_message(output);
}

void Logger::log(const QString &text, const QString &argument)
{
    QString output;
    QString time_span = get_time_span();

    output.append(" <div> ");

    output.append(time_span);

    output.append(" <span> ");
    output.append(text);
    output.append(" </span> ");

    output.append("<b style=\"color:LightGreen;\">");
    output.append(argument);
    output.append("</b>");

    output.append(" </div> ");

    emit send_message(output);
}

void Logger::error(const QString &text)
{
    QString output;
    QString time_span = get_time_span();

    output.append(" <div> ");

    output.append(time_span);

    output.append(" <span style=\"color:red;\"> ");
    output.append(text);
    output.append(" </span> ");

    output.append(" </div> ");
    emit send_message(output);
}

QString Logger::get_current_time_formatted(void)
{
    QTime time = QTime::currentTime();
    int mseconds = time.msec();
    QString time_txt = time.toString() + ":" + QString::number(mseconds);

    return "[ " + time_txt + " ]:    ";
}

QString Logger::get_time_span(void)
{
    QString time = get_current_time_formatted();
    QString result;

    result.append("<b style=\"color:Aquamarine;\"> ");
    result.append(time);
    result.append(" </b> ");

    return result;
}
