#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include<QSerialPortInfo>

#include "lz77.h"
#include "settings.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void update_coms();

private:
    Ui::MainWindow *ui;
    Lz77 lz77;
    Settings settings;

private slots:
    void raw_path_update(const QString &text);
};



#endif // MAINWINDOW_H
