cmake_minimum_required(VERSION 3.5)

project(compressor VERSION 0.1 LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 COMPONENTS Widgets LinguistTools REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Widgets LinguistTools REQUIRED)

find_package(Qt${QT_VERSION_MAJOR} COMPONENTS REQUIRED SerialPort Core)

set(TS_FILES compressor_en_150.ts)

set(PROJECT_SOURCES
        main.cpp

        #views
        mainwindow.cpp
        mainwindow.h
        mainwindow.ui

        #logic
        logic/src/lz77.cpp
        logic/inc/lz77.h
        logic/inc/settings.h
        logic/src/settings.cpp
        logic/inc/lz77_compressor.h
        logic/src/lz77_compressor.cpp
        logic/inc/frame.h
        logic/inc/logger.h
        logic/src/logger.cpp
        ${TS_FILES}
)

if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(compressor
        MANUAL_FINALIZATION
        ${PROJECT_SOURCES}
    )
# Define target properties for Android with Qt 6 as:
#    set_property(TARGET compressor APPEND PROPERTY QT_ANDROID_PACKAGE_SOURCE_DIR
#                 ${CMAKE_CURRENT_SOURCE_DIR}/android)
# For more information, see https://doc.qt.io/qt-6/qt-add-executable.html#target-creation

    qt_create_translation(QM_FILES ${CMAKE_SOURCE_DIR} ${TS_FILES})
else()
    if(ANDROID)
        add_library(compressor SHARED
            ${PROJECT_SOURCES}
        )
# Define properties for Android with Qt 5 after find_package() calls as:
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
    else()
        add_executable(compressor
            ${PROJECT_SOURCES}
        )
    endif()

    qt5_create_translation(QM_FILES ${CMAKE_SOURCE_DIR} ${TS_FILES})
endif()

target_link_libraries(compressor PRIVATE Qt${QT_VERSION_MAJOR}::Widgets)
target_link_libraries(compressor PRIVATE Qt${QT_VERSION_MAJOR}::SerialPort)
target_link_libraries(compressor PRIVATE Qt${QT_VERSION_MAJOR}::Core)

target_include_directories(compressor
    PUBLIC
    "logic/inc"
)

set_target_properties(compressor PROPERTIES
    MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
)

if(QT_VERSION_MAJOR EQUAL 6)
    qt_finalize_executable(compressor)
endif()
